package albion.shabani.channelmessaging.channelmessaging;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.HashMap;

import albion.shabani.channelmessaging.R;

/**
 * Created by shabania on 26/02/2018.
 */
public class LoginActivity extends Activity implements View.OnClickListener,OnDownloadListener{

    private Button buttonValid;
    private EditText editTextId;
    private EditText editTextPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        buttonValid = (Button) findViewById(R.id.buttonValid);
        editTextId = (EditText) findViewById(R.id.editTextId);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        buttonValid.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {


        HttpPostHandler post = new HttpPostHandler();
        post.addOnDownloadListener(this);
        HashMap<String,String> hm = new HashMap<String,String>();
        hm.put("username",editTextId.getText().toString());
        hm.put("password",editTextPassword.getText().toString());

        post.setHttpPostHandler(getApplicationContext());
        post.execute(new PostRequest("http://www.raphaelbischof.fr/messaging/?function=connect",hm));

    }


    @Override
    public void onDownloadComplete(String downloadedContent) {
        Toast.makeText(getApplicationContext(),"Connected",Toast.LENGTH_SHORT).show();
        Gson gson = new Gson();
        Connexion conn = gson.fromJson(downloadedContent, Connexion.class);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("AccessToken", conn.getAccesstoken());
        editor.commit();

        Intent myIntent = new Intent(getApplicationContext(),ChannelListActivity.class);
        startActivity(myIntent);


    }

    @Override
    public void onDownloadError(String error) {
        Toast.makeText(getApplicationContext(),error,Toast.LENGTH_SHORT).show();
    }
}
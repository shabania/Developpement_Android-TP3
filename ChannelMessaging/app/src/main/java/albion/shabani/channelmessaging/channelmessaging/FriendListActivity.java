package albion.shabani.channelmessaging.channelmessaging;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import java.sql.SQLException;

import albion.shabani.channelmessaging.R;

/**
 * Created by shabania on 26/02/2018.
 */
public class FriendListActivity extends Activity {

    private ListView friendList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        friendList = (ListView) findViewById(R.id.listViewFriendList);

        UserDataSource dataSource = new UserDataSource(getApplicationContext());
        try {
            dataSource.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(dataSource.getAllUsers() !=null){
            CustomArrayAdapterFriend adapter = new CustomArrayAdapterFriend(this.getApplicationContext(),dataSource.getAllUsers());
            friendList.setAdapter(adapter);
        }
        dataSource.close();
    }
}
